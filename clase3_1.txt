
clase Producto
	privado definir nombre como caracter
	privado definir precio como real
	privado definir existencia como entero
	
	//***** getter y setter del atributo nombre ******
	publico metodo setNombre(nuevoNombre)
		nombre = nuevoNombre
	fin metodo
	
	publico metodo getNombre()
		retornar nombre
	fin metodo
	//****************************************
	
	//***** getter y setter del atributo existencia ******
	publico metodo setExistencia(nuevoexistencia)
		existencia = nuevoexistencia
	fin metodo
	
	publico metodo getExistencia()
		retornar existencia
	fin metodo
	// ********************************
	
	//****** getter y setter del atributo precio *********
	publico metodo setPrecio(nuevoprecio)
		precio = nuevoprecio
	fin metodo
	
	publico metodo getPrecio()
		retornar precio
	fin metodo
	// *****************************************
	
	publico metodo leerDatos()
		mostrar "Int. el nombre:"
		leer nombre
		
		leerExistencia()
		leerPrecio()
	fin metodo
	
	privado metodo leerExistencia()
		mostrar "Intr. la existencia:"
		leer existencia
	fin metodo
	
	privado metodo leerPrecio()
		mostrar "Intr. el precio:"
		leer precio
	fin metodo
	
	publico metodo calcularMontoInventario()
		retornar existencia*precio
	fin metodo
fin clase

algoritmo clase2
	definir a, b como Producto
	definir monto como real
	definir n como caracter
	
	
	a = nuevo Producto()
	b = nuevo Producto()

	a.setNombre("jose luis")
	a.setExistencia(50)
	a.setPrecio(aleatorio(1000,2000))
	
	mostrar "Introduzca el nombre:"
	leer n
	
	b.setNombre(n)
	b.setPrecio(a.getPrecio())
	b.setExistencia(a.getExistencia()/2)
	

	monto = a.calcularMontoInventario()+
			b.calcularMontoInventario()

	mostrar "El monto del inventario de " 
			"la empresa es ", monto

fin algoritmo















